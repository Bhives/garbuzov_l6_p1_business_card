package com.vironit.garbuzov_l6_p1_business_card

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.vironit.garbuzov_l6_p1_business_card.databinding.ActivityMainBinding
import java.io.File
import kotlin.math.round


private lateinit var binding: ActivityMainBinding
private val PERMISSION_CODE = 88
private val CAMERA_CODE = 99
var currentPath: Uri? = null
var buttonEditSaveToggle: Boolean = false

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        binding.root
        val view = binding.root
        setContentView(view)
        setSupportActionBar(binding.toolbar)
    }

    fun editOrSave(view: View) {
        if (!buttonEditSaveToggle) {
            editButton(view)
        } else {
            saveButton(view)
        }
    }

    private fun editButton(view: View){
        binding.editSaveButton.setText(R.string.save)
        buttonEditSaveToggle=true
        binding.contactPhoto.isClickable=true
        binding.nameTextView.isEnabled=true
        binding.phoneTextView.isEnabled=true
        binding.emailTextView.isEnabled=true
    }

    private fun saveButton(view: View){
        binding.editSaveButton.setText(R.string.edit)
        buttonEditSaveToggle=false
        binding.contactPhoto.isClickable=false
        binding.nameTextView.text = binding.nameTextView.text
        binding.nameTextView.isEnabled=false
        binding.phoneTextView.text = binding.phoneTextView.text
        binding.phoneTextView.isEnabled=false
        binding.emailTextView.text = binding.emailTextView.text
        binding.emailTextView.isEnabled=false
    }

    fun changePhoto(view: View) {
        if (askForPermissions()) {
            takePicture()
        }
    }

    private fun isPermissionsAllowed(): Boolean {
        return ContextCompat.checkSelfPermission(
            this, Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            this, Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun askForPermissions(): Boolean {
        if (!isPermissionsAllowed()) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this as Activity,
                    Manifest.permission.CAMERA
                ) && ActivityCompat.shouldShowRequestPermissionRationale(
                    this as Activity,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                )
            ) {
                showPermissionDeniedDialog()
            } else {
                ActivityCompat.requestPermissions(
                    this as Activity,
                    arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE),
                    PERMISSION_CODE
                )
            }
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    takePicture()
                } else {
                    askForPermissions()
                }
                return
            }
        }
    }

    private fun showPermissionDeniedDialog() {
        AlertDialog.Builder(this)
            .setTitle("Access denied")
            .setMessage("Access denied. Please allow permissions from App Settings.")
            .setPositiveButton("App Settings",
                DialogInterface.OnClickListener { dialogInterface, i ->
                    val intent = Intent()
                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    val uri = Uri.fromParts("package", packageName, null)
                    intent.data = uri
                    startActivity(intent)
                })
            .setNegativeButton("Cancel", null)
            .show()
    }

    private fun takePicture() {
        val photoValues = ContentValues()
        photoValues.put(MediaStore.Images.Media.TITLE, "New Picture")
        photoValues.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        photoValues.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg")
        currentPath = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, photoValues)
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, currentPath)
        startActivityForResult(cameraIntent, CAMERA_CODE)
    }

    fun call(view: View) {
        if(binding.phoneTextView.text.isEmpty()){
            Toast.makeText(
                this,
                "Phone field must be not empty",
                Toast.LENGTH_SHORT
            ).show()
        }
        else {
            val dialIntent = Intent(Intent.ACTION_DIAL)
            dialIntent.data = Uri.parse("tel:" + binding.phoneTextView.text)
            startActivity(dialIntent)
        }
    }

    fun sendEmail(view: View){
        if(binding.emailTextView.text.isEmpty()){
            Toast.makeText(
                this,
                "Email field must be not empty",
                Toast.LENGTH_SHORT
            ).show()
        }
        else{
            val emailIntent = Intent(Intent.ACTION_SENDTO)
            emailIntent.data = Uri.parse("mailto:")
            emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(binding.emailTextView.text.toString()))
            startActivity(emailIntent);
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK){
            binding.contactPhoto.setImageURI(currentPath)
        }
    }
}